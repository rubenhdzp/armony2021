#!/bin/sh

echo APP_NAME=Armony >> .env
echo APP_ENV=production >> .env
echo APP_KEY=$APP_KEY >> .env
echo APP_DEBUG=false >> .env
echo APP_URL=https://www.marivalarmony.com >> .env
echo DEBUGBAR_ENABLED=false >> .env
echo LOG_CHANNEL=stack >> .env

echo DB_CONNECTION=mysql >> .env
echo DB_HOST=armony_db >> .env
echo DB_PORT=3306 >> .env
echo DB_DATABASE=$DB_DATABASE >> .env
echo DB_USERNAME=$DB_USERNAME >> .env
echo DB_PASSWORD=$DB_PASSWORD >> .env

echo SENTRY_LARAVEL_DSN=$SENTRY_LARAVEL_DSN >> .env
echo SENTRY_TRACES_SAMPLE_RATE=1 >> .env
echo SENTRY_ENVIRONMENT=production >> .env

echo URL_API_PRICES=$URL_API_PRICES >> .env

echo PRICE_TRAVEL_HOTEL_ID=$PRICE_TRAVEL_HOTEL_ID >> .env
echo PRICE_TRAVEL_USER_EN=$PRICE_TRAVEL_USER_EN >> .env
echo PRICE_TRAVEL_PASSWORD_EN=$PRICE_TRAVEL_PASSWORD_EN >> .env
echo PRICE_TRAVEL_USER_ES=$PRICE_TRAVEL_USER_ES >> .env
echo PRICE_TRAVEL_PASSWORD_ES=$PRICE_TRAVEL_PASSWORD_ES >> .env

echo HOST_DEEP_LINK_EN=$HOST_DEEP_LINK_EN >> .env
echo HOST_DEEP_LINK_ES=$HOST_DEEP_LINK_ES >> .env

echo MAIL_DRIVER= $MAIL_DRIVER >> .env
echo POSTMARK_TOKEN= $POSTMARK_TOKEN >> .env
echo MAIL_FROM_ADDRESS= $MAIL_FROM_ADDRESS >> .env

echo GOOGLE_RECAPTCHA_SITE_KEY= $GOOGLE_RECAPTCHA_SITE_KEY >> .env
echo GOOGLE_RECAPTCHA_API_SECRET= $GOOGLE_RECAPTCHA_API_SECRET >> .env

echo MAILCHIMP_USER= $MAILCHIMP_USER >> .env 
echo MAILCHIMP_TOKEN= $MAILCHIMP_TOKEN >> .env
echo MAILCHIMP_LIST_ID_ES= $MAILCHIMP_LIST_ID_ES >> .env
echo MAILCHIMP_LIST_ID_EN= $MAILCHIMP_LIST_ID_EN >> .env