{{ App::setLocale($data->lang) }}
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <meta name="x-apple-disable-message-reformatting">
  <title></title>
  <!--[if mso]>
  <noscript>
    <xml>
      <o:OfficeDocumentSettings>
        <o:PixelsPerInch>96</o:PixelsPerInch>
      </o:OfficeDocumentSettings>
    </xml>
  </noscript>
  <![endif]-->
  <style>
    table, td, div, h1, p {font-family: Arial, sans-serif;}
  </style>
</head>
<body style="margin:0;padding:0;">
  <table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;background:#ffffff;">
    <tr>
      <td align="center" style="padding:0;">
        <table role="presentation" style="width:602px;border-collapse:collapse;border:1px solid #cccccc;border-spacing:0;text-align:left;">
          <tr>
            <td align="center" style="padding:20px 0 30px 0;background:#3ec4e3;">
              <img src="https://experiencesresorts.com/img/logo2.svg" alt="" width="200" style="height:auto;display:block;" />
            </td>
          </tr>
          <tr>
            <td style="padding:16px 30px 42px 30px;">
              <table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;">
                <tr>
                  <td style="padding:0 0 5px 0;color:#153643;">
                    <h1 style="font-size:24px;margin:0 0 10px 0;font-family:Arial,sans-serif;">Resort Room</h1>
                  </td>
                </tr>
				<tr>
                  <td style="padding:0 0 16px 0;color:#153643;">
                    <img src="https://www.marivalemotions.com/mediafiles/pages/2019/07/3/header-accomodatons-1920x700-04.jpg" width="100%">
                  </td>
                </tr>
                <tr>
                  <td style="padding:0;">
                    <table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;">
						<tr>
							<td colspan=3>
								<table style="width:100%;border:1px solid #ddf4fa; padding:5px; font-size:13px;border-collapse:collapse;" border="0"  cellspacing="0" cellpadding="0">
									<thead>
										<tr style="background-color: #ddf4fa;">
											<th style="padding:10px; font-size:12px; color:#494949; text-align:center;">Week</th> 
											<th style="padding:10px; font-size:12px; color:#494949; text-align:center;">Mon</th>
											<th style="padding:10px; font-size:12px; color:#494949; text-align:center;">Tue</th>
											<th style="padding:10px; font-size:12px; color:#494949; text-align:center;">Wed</th>
											<th style="padding:10px; font-size:12px; color:#494949; text-align:center;">Thu</th>
											<th style="padding:10px; font-size:12px; color:#494949; text-align:center;">Fri</th>
											<th style="padding:10px; font-size:12px; color:#494949; text-align:center;">Sat</th>
											<th style="padding:10px; font-size:12px; color:#494949; text-align:center;">Sun</th>
										</tr>
									</thead> 
									<tbody>
										<tr>
											<td style="border:1px solid #ddf4fa; padding:5px; font-size:13px; text-align:center;">1</td> 
											<td style="border:1px solid #ddf4fa; padding:5px; font-size:13px">
												<span style="color: #b1b1b1;background-color: #eee;border: solid 1px #ccc;border-radius: 4px;font-size: 11px;line-height: 25px;padding: 2px;">Ago 30</span><br />
												<span style="color: #b1b1b1; text-decoration:line-through;">$439.22</span><br />
												<span>$203.79</span>
											</td>
											<td style="border:1px solid #ddf4fa; padding:5px; font-size:13px">
												<span style="color: #b1b1b1;background-color: #eee;border: solid 1px #ccc;border-radius: 4px;font-size: 11px;line-height: 25px;padding: 2px;">Ago 31</span><br />
												<span style="color: #b1b1b1; text-decoration:line-through;">$439.22</span><br />
												<span>$203.79</span>
											</td>
											<td style="border:1px solid #ddf4fa; padding:5px; font-size:13px">
												<span style="color: #b1b1b1;background-color: #eee;border: solid 1px #ccc;border-radius: 4px;font-size: 11px;line-height: 25px;padding: 2px;">Sep 01</span><br />
												<span style="color: #b1b1b1; text-decoration:line-through;">$398.88</span><br />
												<span>$185.07</span>
											</td>
											<td style="border:1px solid #ddf4fa; padding:5px; font-size:13px">
												<span style="color: #b1b1b1;background-color: #eee;border: solid 1px #ccc;border-radius: 4px;font-size: 11px;line-height: 25px;padding: 2px;">Sep 02</span><br />
												<span style="color: #b1b1b1; text-decoration:line-through;">$398.88</span><br />
												<span>$185.07</span>
											</td>
											<td style="border:1px solid #ddf4fa; padding:5px; font-size:13px">
												<span style="color: #b1b1b1;background-color: #eee;border: solid 1px #ccc;border-radius: 4px;font-size: 11px;line-height: 25px;padding: 2px;">Sep 03</span><br />
												<span style="color: #b1b1b1; text-decoration:line-through;">$436.97</span><br />
												<span>$202.75</span>
											</td>
											<td style="border:1px solid #ddf4fa; padding:5px; font-size:13px">
												<span style="color: #b1b1b1;background-color: #eee;border: solid 1px #ccc;border-radius: 4px;font-size: 11px;line-height: 25px;padding: 2px;">Sep 04</span><br />
												<span style="color: #b1b1b1; text-decoration:line-through;">$436.97</span><br />
												<span>$202.75</span>
											</td>
											<td style="border:1px solid #ddf4fa; padding:5px; font-size:13px">
												<span style="color: #b1b1b1;background-color: #eee;border: solid 1px #ccc;border-radius: 4px;font-size: 11px;line-height: 25px;padding: 2px;">Sep 05</span><br />
												<span style="color: #b1b1b1; text-decoration:line-through;">$398.88</span><br />
												<span>$185.07</span>
											</td>
										</tr>
										<tr>
											<td style="border:1px solid #ddf4fa; padding:5px; font-size:13px; text-align:center;">2</td>
											<td style="border:1px solid #ddf4fa; padding:5px; font-size:13px">
												<span style="color: #b1b1b1;background-color: #eee;border: solid 1px #ccc;border-radius: 4px;font-size: 11px;line-height: 25px;padding: 2px;">Sep 06</span><br />
												<span style="color: #b1b1b1; text-decoration:line-through;">$398.88</span><br />
												<span>$185.07</span>
											</td>
											<td style="border:1px solid #ddf4fa; padding:5px; font-size:13px">
												<span style="color: #b1b1b1;background-color: #eee;border: solid 1px #ccc;border-radius: 4px;font-size: 11px;line-height: 25px;padding: 2px;">Sep 07</span><br />
												<span style="color: #b1b1b1; text-decoration:line-through;">$398.88</span><br />
												<span>$185.07</span>
											</td>
											<td style="border:1px solid #ddf4fa; padding:5px; font-size:13px">
												<span style="color: #b1b1b1;background-color: #eee;border: solid 1px #ccc;border-radius: 4px;font-size: 11px;line-height: 25px;padding: 2px;">Sep 08</span><br />
												<span style="color: #b1b1b1; text-decoration:line-through;">$398.88</span><br />
												<span>$185.07</span>
											</td>
											<td style="border:1px solid #ddf4fa; padding:5px; font-size:13px">
												<span style="color: #b1b1b1;background-color: #eee;border: solid 1px #ccc;border-radius: 4px;font-size: 11px;line-height: 25px;padding: 2px;">Sep 09</span><br />
												<span style="color: #b1b1b1; text-decoration:line-through;">$398.88</span><br />
												<span>$185.07</span>
											</td>
											<td style="border:1px solid #ddf4fa; padding:5px; font-size:13px"><span></span>&nbsp;<span></span></td>
											<td style="border:1px solid #ddf4fa; padding:5px; font-size:13px"><span></span>&nbsp; <span></span></td>
											<td style="border:1px solid #ddf4fa; padding:5px; font-size:13px"><span></span>&nbsp; <span></span></td>
										</tr>
									</tbody>
								</table>
							<td>
						</tr>
						<tr>
							<td colspan=3 style="padding: 15px 0px;">
								<table>
									<tr>
										<td style="background-color: #96daea!important; border:1px solid #96daea; border-radius: 4px; color: #001a70!important; font-size: 13px; padding: 15px 20px;">
											61% off, Fun for the whole family at a special rate + $200 USD Resort Credit with a minimum 3 night stay 
											Marival Resorts is pleased to offer free on-site CDC compliant Covid19 testing to all US guests with a stay of 3 nights or more!).
										</td>
									</tr>
								</table>
							<td>
						</tr>
						<tr>
							<td style="width:310px;padding:0;font-size:0;line-height:0;">&nbsp;</td>
							<td style="width:130px;padding:5px; text-align:right; font-weight:bold">
								Room (1) total:
							</td>
							<td style="width:70px;padding:5px;vertical-align:top;color:#153643; text-align:right; font-weight:bold">
								$2,108.58
							</td>
						</tr>
						<tr>
							<td style="width:310px;padding:0;font-size:0;line-height:0;">&nbsp;</td>
							<td style="width:130px;padding:5px; text-align:right; font-weight:bold">
								Total:
							</td>
							<td style="width:70px;padding:5px;vertical-align:top;color:#153643; text-align:right; font-weight:bold">
								$2,108.58
							</td>
						</tr>
						<tr>
							<td style="width:310px;padding:0;font-size:0;line-height:0;">&nbsp;</td>
							<td colspan=2 style="width:150px;padding:10px 0px;vertical-align:top;color:#153643; text-align:right;">
								*Rates per night in USD
							</td>
						</tr>
						<tr>
							<td style="width:310px;padding:0;font-size:0;line-height:0;">&nbsp;</td>
							<td colspan=2 style="width:150px;padding:0;vertical-align:top;color:#153643; text-align:right; padding-top:15px">
								<a style="border:1px solid #96daea; border-radius: 4px; font-size:18px; background-color:#96daea; color: #001a70; padding:10px 25px; text-decoration:none;" href="https://book.marivalemotions.com/book/reservation-details?idRoom=1000377&amp;idRate=30338&amp;idHotel=100371&amp;Rooms=1&amp;CheckIn=08%2F30%2F2021&amp;CheckOut=09%2F10%2F2021&amp;Adults=2&amp;Kids=0&amp;Room0.Adults=2&amp;Room0.Kids=0">
									Select room
								</a>
							</td>
						</tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td style="padding:20px 30px;background:#3ec4e3;">
              <table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;font-size:9px;font-family:Arial,sans-serif;">
                <tr>
                  <td style="padding:0;width:50%;" align="left">
                    
					<p style="margin:0;font-size:14px;line-height:16px;font-family:Arial,sans-serif;color:#ffffff; font-size:11px">
						Blvd. Paseo Cocoteros S/N Nuevo Vallarta,<br />
						Nayarit, México 63732<br />
						Reservations: US/CAN: <a href="tel:8002704980" style="color: #001a70; text-decoration:none">+1 (888) 270 4980</a><br />
						México Lada sin Costo: <a href="tel:8004390212" style="color: #001a70; text-decoration:none">(800) 439 0212</a><br />
						Hotel: <a href="tel:+523292917000" style="color: #001a70; text-decoration:none">+52 (329) 291-7000</a>
					</p>
                  </td>
                  <td style="padding:0;width:50%;" align="right">
                    <table role="presentation" style="border-collapse:collapse;border:0;border-spacing:0;">
                      <tr>
                        <td style="padding:0 0 0 10px;width:38px;">
							<a href="https://www.facebook.com/MarivalEmotions/" target="_blank">
								<svg style="color:#2b899e; width:17px" aria-hidden="true" focusable="false" data-prefix="fab" data-icon="facebook-f" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" data-fa-i2svg=""><path fill="currentColor" d="M279.14 288l14.22-92.66h-88.91v-60.13c0-25.35 12.42-50.06 52.24-50.06h40.42V6.26S260.43 0 225.36 0c-73.22 0-121.08 44.38-121.08 124.72v70.62H22.89V288h81.39v224h100.17V288z"></path></svg>
							</a>
                        </td>
                        <td style="padding:0 0 0 10px;width:38px;">
							<a href="https://www.instagram.com/marivalemotions/" target="_blank">
								<svg style="color:#2b899e; width:30px" aria-hidden="true" focusable="false" data-prefix="fab" data-icon="instagram" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M224.1 141c-63.6 0-114.9 51.3-114.9 114.9s51.3 114.9 114.9 114.9S339 319.5 339 255.9 287.7 141 224.1 141zm0 189.6c-41.1 0-74.7-33.5-74.7-74.7s33.5-74.7 74.7-74.7 74.7 33.5 74.7 74.7-33.6 74.7-74.7 74.7zm146.4-194.3c0 14.9-12 26.8-26.8 26.8-14.9 0-26.8-12-26.8-26.8s12-26.8 26.8-26.8 26.8 12 26.8 26.8zm76.1 27.2c-1.7-35.9-9.9-67.7-36.2-93.9-26.2-26.2-58-34.4-93.9-36.2-37-2.1-147.9-2.1-184.9 0-35.8 1.7-67.6 9.9-93.9 36.1s-34.4 58-36.2 93.9c-2.1 37-2.1 147.9 0 184.9 1.7 35.9 9.9 67.7 36.2 93.9s58 34.4 93.9 36.2c37 2.1 147.9 2.1 184.9 0 35.9-1.7 67.7-9.9 93.9-36.2 26.2-26.2 34.4-58 36.2-93.9 2.1-37 2.1-147.8 0-184.8zM398.8 388c-7.8 19.6-22.9 34.7-42.6 42.6-29.5 11.7-99.5 9-132.1 9s-102.7 2.6-132.1-9c-19.6-7.8-34.7-22.9-42.6-42.6-11.7-29.5-9-99.5-9-132.1s-2.6-102.7 9-132.1c7.8-19.6 22.9-34.7 42.6-42.6 29.5-11.7 99.5-9 132.1-9s102.7-2.6 132.1 9c19.6 7.8 34.7 22.9 42.6 42.6 11.7 29.5 9 99.5 9 132.1s2.7 102.7-9 132.1z"></path></svg>
							</a>
                        </td>
						<td style="padding:0 0 0 10px;width:38px;">
							<a href="https://www.youtube.com/channel/UCwGVkbVwFOLefTrepSp-aRg" target="_blank">
							<svg style="color:#2b899e; width:45px" aria-hidden="true" focusable="false" data-prefix="fab" data-icon="youtube" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M549.655 124.083c-6.281-23.65-24.787-42.276-48.284-48.597C458.781 64 288 64 288 64S117.22 64 74.629 75.486c-23.497 6.322-42.003 24.947-48.284 48.597-11.412 42.867-11.412 132.305-11.412 132.305s0 89.438 11.412 132.305c6.281 23.65 24.787 41.5 48.284 47.821C117.22 448 288 448 288 448s170.78 0 213.371-11.486c23.497-6.321 42.003-24.171 48.284-47.821 11.412-42.867 11.412-132.305 11.412-132.305s0-89.438-11.412-132.305zm-317.51 213.508V175.185l142.739 81.205-142.739 81.201z"></path></svg>
							</a>
						</td>
                      </tr>
                    </table>
                  </td>
                </tr>
				<tr>
					<td colspan=2>
						<p style="margin:10px; padding-top:10px; font-size:14px;line-height:16px;font-family:Arial,sans-serif;color:#ffffff; text-align:center">
						  Todos los derechos reservados para Marival® Group 2021
						</p>
					<td>
				</tr>	
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</body>
</html>