FROM alpine:edge

COPY repositories /etc/apk/repositories

WORKDIR /var/www

# Essentials
RUN echo "UTC" > /etc/timezone
RUN apk add --no-cache zip unzip curl sqlite git

# Installing bash
RUN apk add bash
RUN sed -i 's/bin\/ash/bin\/bash/g' /etc/passwd

# Installing PHP
RUN apk add --no-cache php \
  php-common \
  php-fpm \
  php-pdo \
  php-opcache \
  php-zip \
  php-phar \
  php-iconv \
  php-cli \
  php-curl \
  php-openssl \
  php-mbstring \
  php-tokenizer \
  php-fileinfo \
  php-json \
  php-xml \
  php-xmlwriter \
  php-xmlreader \
  php-simplexml \
  php-dom \
  php-pdo_mysql \
  php-pdo_sqlite \
  php-tokenizer \
  php7-pecl-redis \
  php-ctype \
  php-gd \
  php-calendar

# Installing composer
RUN curl -sS https://getcomposer.org/installer -o composer-setup.php
RUN php composer-setup.php --install-dir=/usr/local/bin --filename=composer
RUN rm -rf composer-setup.php

COPY www.conf /etc/php7/php-fpm.d/www.conf

RUN set -x ; \
  addgroup -g 83 -S www ; \
  adduser -u 83 -D -S -G www www && exit 0 ; exit 1

COPY . /var/www/

# Build process
#RUN composer install --no-dev

# Copy existing application directory permissions
COPY --chown=www:www . /var/www
# Container execution
EXPOSE 9000
CMD ["php-fpm7", "-F"]
